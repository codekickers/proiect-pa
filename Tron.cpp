/* Nume Echipa : Studentii au cod lent
 * Membri : Ablachim Kayhan, Velciu Veronica, Oprea Matei, Grindeanu Ciprian
 * Grupa : 322CA
 */

#include <iostream>
#include <vector>
#include <queue>
#define MAXDEPTH        7
#define INF             9999

using namespace std;

/* Vectori de distante relative fata de punctul curent.
 * 0 - stanga
 * 1 - dreapta
 * 2 - sus
 * 3 - jos
 */
int dx[4] = {0, 0, -1, 1};
int dy[4] = {-1, 1, 0, 0};

string moves[4] = {"LEFT", "RIGHT", "UP", "DOWN"};

/* Verifica daca patratul de la linia x si coloana y se afla in interiorul
 * tablei.
 */
int isInTable(int x, int y, vector<string> &board) {
    if (x > 0 && y > 0 && (unsigned int) x < board.size() &&
            (unsigned int) y < board[0].length())
        return 1;
    return 0;
}

/* Exploreaza harta din pozitia x, y, considerand coordonatele adversarului
 * o_x, o_y. Construieste in distances o matrice cu timpul in care ajunge botul
 * la pozitia [i][j], considerand ca adversarul ar face aceleasi mutari in 
 * oglinda.*/
queue <pair <int, int> > q; // Coada de parcurgere
queue <int> times; // Timpii de explorare

void explore(int x, int y, int o_x, int o_y, vector<string> &board,
        vector< vector<int> > &distances, int steps) {
    /* Marcheaza nodurile vecine pentru a fi vizitate. */
    for (int i = 0; i < 4; i++)
        if (board[x + dx[i]][y + dy[i]] == '-' ||
                (distances[x + dx[i]][y + dy[i]] > steps &&
                board[x + dx[i]][y + dy[i]] == '*')) {
            q.push(make_pair(x + dx[i], y + dy[i]));
            times.push(steps);
            board[x + dx[i]][y + dy[i]] = '@';
        }

    /* Exploreaza nodurile marcate (BFS) */
    for (int i = 0; i < 4; i++) {
        if (board[x + dx[i]][y + dy[i]] == '@') {
            board[x + dx[i]][y + dy[i]] == '*';
            if (isInTable(o_x - dx[i], o_y - dy[i], board))
                if (board[o_x - dx[i]][o_y - dy[i]] == '-') {
                    board[o_x - dx[i]][o_y - dy[i]] = '*';
                    o_x -= dx[i];
                    o_y -= dy[i];
                }
            while (!q.empty()) {
                pair <int, int> first = q.front();
                int t = times.front();
                distances[first.first][first.second] = t + 1;
                q.pop();
                times.pop();
                explore(first.first, first.second, o_x, o_y,
                        board, distances, t + 1);
            }
        }
    }
}

/* Returneaza true daca este punct de articulatie si false altfel*/
bool punct_de_art(int x , int y , vector<string> &board) {
	if((board[x+1][y]=='-'&&board[x-1][y]=='-')||(board[x][y+1]=='-'&&board[x][y-1]=='-')) 
		return true;
	return false;
	}

	
void marcheaza_articulatii(int x, int y, vector<string> &board,
			vector< vector<int> > &distances) {
	/* Marcheaza nodurile vecine pentru a fi vizitate. */
	for(int i = 0; i < 4; i++)
		if(board[x + dx[i]][y + dy[i]] == '-' ){
			q.push(make_pair(x + dx[i],y + dy[i]));
			board[x + dx[i]][y + dy[i]] = '@';
		}

	/* Exploreaza nodurile marcate (BFS) */
	for(int i = 0; i < 4; i++) {
			while (!q.empty()){
				pair <int , int> first = q.front();
				/*daca e punct de articulatie pun punct in vectorul board*/
				if(punct_de_art(first.first,first.second)) {
					board[first.first][first.second]='.';
					/*se marcheaza si punctele dinainte si dupa punctele de articulatie*/
					board[x][y]='.';
					}
				q.pop();
				marcheaza_articulatii(first.first, first.second, 
                                        board);
			}
		}
	}


/* Verifica daca jucatorii se afla in aceeasi componenta conexa */
void connected(int x, int y, int o_x, int o_y, vector<string> &board, int &found) {

    for (int i = 0; i < 4; i++) {
        if (board[x + dx[i]][y + dy[i]] == '-') {
            if (x + dx[i] == o_x && y == y + dy[i]) {
                found = 1;
                return;
            }
            board[x + dx[i]][y + dy[i]] = '*';
            connected(x + dx[i], y + dy[i], o_x, o_y, board, found);
        }
    }
}

int survival_mode(int x, int y, int o_x, int o_y, vector<string> board) {

    int best_move = 0;

    /* cat timp in dreapta am perete incerc sa ocup tot spatiul */
    if (board[x + dx[1]][y + dy[1]] != '-') {
        if (board[x + dx[2]][y + dy[2]] == '-') {
            /*best_move va fi = 2 Adica mergem in fata*/
            best_move = 2;
        }/*Daca in fata gasim # - perete */
        else if (board[x + dx[2]][y + dy[2]] != '-') {
            /*Altfel incercam sa facem mutare la stanga*/
            if (board[x + dx[0]][y + dy[0]] == '-')
                best_move = 0;
        } else
            best_move = 3;
    }/*Daca avem perete in sus*/
    else
        if (board[x + dx[2]][y + dy[2]] != '-') {
        /*Daca putem face mutare in dreapta o facem */
        if (board[x + dx[1]][y + dy[1]] == '-') {
            best_move = 1;
        }/*Altfel facem stanga*/
        else
            best_move = 0;
    }/*Daca am perete in stanga*/
    else
        if (board[x + dx[0]][y + dy[0]] != '-') {
        /*Daca pot merge in sus*/
        if (board[x + dx[2]][y + dy[2]] == '-')
            best_move = 2;
            /*Daca pot merge in dreapta*/
        else if (board[x + dx[1]][y + dy[1]] == '-')
            best_move = 1;
            /*Altfel o iau in jos*/
        else
            best_move = 3;

    }


    return best_move;
}

/* Sterge marcajele '*' de pe tabla.*/
void removeMarks(vector <string> &board) {
    for (unsigned int i = 0; i < board.size(); i++)
        for (unsigned int j = 0; j < board[i].length(); j++)
            if (board[i][j] == '*')
                board[i][j] = '-';
}

/* Evalueaza o configuratie a tablei folosind euristica Voronoi */
int evaluate(int x, int y, int o_x, int o_y, vector<string> board) {
    int h = board.size();
    int w = board[0].length();

    /* Matricea distantelor pentru bot si adversar + initializare */
    vector < vector<int> > dist_o;
    vector < vector<int> > dist;
    for (unsigned int i = 0; i < board.size(); i++) {
        vector<int> d(w, INF);
        for (int j = 0; j < h; j++) {
            dist.push_back(d);
            dist_o.push_back(d);
        }
    }
    /* Copiere tabla */
    vector<string> newboard(h, "");
    for (int i = 0; i < h; i++)
        newboard[i] = board[i];

    explore(x, y, o_x, o_y, board, dist, 0);
    removeMarks(board);

    explore(o_x, o_y, x, y, newboard, dist_o, 0);
    removeMarks(newboard);

    int count = 0, count_o = 0;
    for (int i = 0; i < h; i++)
        for (int j = 0; j < w; j++) {
            if (dist[i][j] < dist_o[i][j])
                count++;
            if (dist[i][j] > dist_o[i][j])
                count_o++;
        }
    return count - count_o;
}

/* Algoritm alpha-beta prunning */
int alphaBeta(int alpha, int beta, int depthleft,
        int x, int y, int o_x, int o_y, vector<string> &board) {

    if (depthleft == 0) {
        return evaluate(x, y, o_x, o_y, board);
    }

    int score;
    for (int i = 0; i < 4; i++) {
        if (board[x + dx[i]][y + dy[i]] == '-') {
            board[x + dx[i]][y + dy[i]] = 'x';
            score = -alphaBeta(-beta, -alpha, depthleft - 1,
                    o_x, o_y, x + dx[i], y + dy[i], board);
            board[x + dx[i]][y + dy[i]] = '-';
        } else
            score = -INF;
        if (score >= beta)
            return beta;
        if (score > alpha)
            alpha = score;

    }

    return alpha;
}

int around(int x, int y, int o_x, int o_y) {
    for(int i = 0; i < 4; i++)
        if(x + dx[i] == o_x && y + dy[i] == o_y)
            return 1;
    return 0;
}

void nextMove(char player, int x, int y, int o_x, int o_y,
        vector<string> board) {

    int best_move = 0;
    int best_count = -INF, count;

    /* x si y reprezinta coordonatele botului
     * o_x si o_y reprezinta coordonatele adversarului
     */
    if (player == 'g') {
        int aux = x;
        x = o_x;
        o_x = aux;
        aux = y;
        y = o_y;
        o_y = aux;
    }

    int con = 0;
    connected(x, y, o_x, o_y, board, con);
    removeMarks(board);

    if (around(x, y, o_x, o_y))
        best_move = survival_mode(x, y, o_x, o_y, board);
    else if (con) {
        /* Calculam care din cele 4 mutari posibile este mai favorabila	 */
        for (int i = 0; i < 4; i++) {
            if (board[x + dx[i]][y + dy[i]] == '-') {
                x += dx[i];
                y += dy[i];
                board[x][y] = '#';
                count = -alphaBeta(-INF, INF, MAXDEPTH,
                        o_x, o_y, x, y, board);
                if (count > best_count) {
                    best_count = count;
                    best_move = i;
                }
                board[x][y] = '-';
                x -= dx[i];
                y -= dy[i];

            }
        }
    } else
        best_move = survival_mode(x, y, o_x, o_y, board);
    cout << moves[best_move];
}

int main() {

    char player;
    int x, y;
    int o_x, o_y;
    int size_x, size_y;
    vector <string> board;

    cin >> player;
    cin >> x >> y >> o_x >> o_y >> size_x >> size_y;
    for (int i = 0; i < size_x; i++) {
        string s;
        cin >> s;
        board.push_back(s);
    }

    nextMove(player, x, y, o_x, o_y, board);

    return 0;
}
