# Studentii au cod lent	322CA
CC = g++
CCFLAGS = -Wall -lm -g
SRC = Tron.cpp
PROGRAM = tron

build:
	$(CC) $(SRC) -o $(PROGRAM) $(CCFLAGS)

.PHONY: clean
clean:
	rm -f $(PROGRAM) core *~